<?php

namespace App\Controllers;
use App\Models\UsersModel;

class Home extends BaseController
{
	public function index()
	{
            $security = \Config\Services::security();
            
		return view('welcome_message');
	}
        
        public function giris()
        {
            helper('form');
            
            if($this->request->getMethod()=='post')
            {
                $rules = [
                    'mail' => "required|valid_email",
                    'sifre' => 'required|min_length[4]|validateUser[mail,sifre]',
                ];
                $errors = [
                            'sifre' => [
                                'validateUser' => 'Mail adresi veya Şifre uyuşmuyor !'
                            ]
			];
                
                /*$new = ["email"=> $this->request->getPost('mail'),'sifre'=> $this->request->getPost('sifre')];
                $d = new UsersModel();
                $d->save($new);*/
                if(! $this->validate($rules,$errors))
                {
                    $data['validation'] = $this->validator;
                }else
                {
                    return redirect()->to('dashboard');
                }
            }
            return view('welcome_message',$data);
        }
}
