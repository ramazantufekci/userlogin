<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\UsersModel;
/**
 * Description of Dashboard
 *
 * @author Ramazan TÜFEKÇİ
 */
class Dashboard extends BaseController{
    public function index()
    {
        return view('dashboard/dashboard');
    }
    
    public function olustur()
    {
        helper('form');
        
        if($this->request->getMethod()=='post')
        {
            $model = new UsersModel();
            $model->save($_POST);
            
            $rules = [
                'adi' => "required",
                'mail' => "required|valid_email",
                'sifre' => 'required|min_length[4]',
                ];
                $errors = [
                            'sifre' => [
                                'validateUser' => 'Mail adresi veya Şifre uyuşmuyor !'
                            ]
		];
        }
        return view('dashboard/olustur');
    }
}
