<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;
use CodeIgniter\Model;
/**
 * Description of UsersModel
 *
 * @author Ramazan TÜFEKÇİ
 */
class UsersModel extends Model{
    protected $table = 'kullanici';
    protected $allowedFields = ['adi','email','sifre','durum','yetki','updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
    
    protected function beforeInsert(array $data)
    {
        $data = $this->passwordHash($data);
        $data['data']['created_at'] = date('Y-m-d H:i:s');
        return $data;
    }
    
    protected function beforeUpdate(array $data)
    {
        $data = $this->passwordHash($data);
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }
    
    protected function passwordHash(array $data){
        if(isset($data['data']['sifre']))
          $data['data']['sifre'] = password_hash($data['data']['sifre'], PASSWORD_DEFAULT);

        return $data;
    }
}
