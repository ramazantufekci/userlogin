<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validation;
use App\Models\UsersModel;

/**
 * Description of UserRules
 *
 * @author Ramazan TÜFEKÇİ
 */
class UserRules {
    public function validateUser(string $str,string $fields, array $data) {
        $model = new UsersModel();
        $user = $model->where('email',$data['mail'])->first();
        if(!$user)
        {
            return false;
        }
        return password_verify($data['sifre'], $user['sifre']);
        
    }
}
