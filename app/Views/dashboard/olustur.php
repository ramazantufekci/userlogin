<?php
$this->extend('layout/main');

$this->section('content');
?>
      
<div class="container">
    <div class="row">
        <?php echo $this->include('layout/sidebar');?>
        <div class="col-md-8">
            <form action="/dashboard/olustur" method="post">
                <div class="form-group mb-3">
                    <label id="kullaniciAdi">Kullanıcı Adı</label>
                    <input type="text" name="adi" class="form-control" id="kullaniciAdi">
                </div>
                <div class="form-group mb-3">
                    <label id="kullaniciMail">Mail Adresi</label>
                    <input type="email" name="email" class="form-control" id="kullaniciMail">
                </div>
                <div class="form-group mb-3">
                    <label id="kullaniciSifre">Şifre</label>
                    <input type="password" name="sifre" class="form-control" id="kullaniciSifre">
                </div>
                <div class="form-group mb-3">
                    <label id="kullaniciYetki">Yetki</label>
                    <select class="form-select" aria-label="Default select example" id="kullaniciYetki" name="yetki">
                        <option selected>Yetki Seçin</option>
                        <option value="0">Admin</option>
                        <option value="1">Kullanici</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label id="kullaniciDurum">Durum</label>
                    <select class="form-select" aria-label="Default select example" id="kullaniciDurum" name="durum">
                        <option selected>Kullanıcı Durumunu Seçin</option>
                        <option value="0">Etkin</option>
                        <option value="1">Devredışı</option>
                    </select>
                </div>
                <div class="form-group mb-3">
                    <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                    <button type="submit" class="btn btn-primary display-block">Kullanıcı Oluştur</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->endSection();?>

