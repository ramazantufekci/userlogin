<?php
$this->extend('layout/main');
$this->section('content');
?>
<div class="container">
    <div class="overlay">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                     <?php if (isset($validation))
                     {?>
                    <div class="alert alert-danger" role="alert">
                        <?php
                         echo $validation->listErrors();
                     ?>
                    </div>
                    <?php } ?>
                    <h1 class="mb-3 text-center bg-primary text-light">Giriş Ekranı</h1>
                    <form action="/giris" method="POST" >
                    <div class="mb-3">
                        <label for="mailInput" class="form-label">Mail Adresi</label>
                        <input type="email" class="form-control" id="mailInput" name="mail" placeholder="name@example.com" required>
                    </div>
                    <div class="mb-3">
                      <label for="sifreInput" class="form-label">Şifre</label>
                      <input type="password" class="form-control" name="sifre" id="sifreInput" required>
                    </div>
                    <div class="mb-3">
                        <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
                        <button type="submit" class="form-control btn btn-primary" >Giriş Yap</button>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->endSection();?>